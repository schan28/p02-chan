//
//  AppDelegate.h
//  p02-chan
//
//  Created by Stanley Chan on 2/2/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

