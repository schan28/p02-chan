//
//  main.m
//  p02-chan
//
//  Created by Stanley Chan on 2/2/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
