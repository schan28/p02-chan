//
//  ViewController.h
//  p02-chan
//
//  Created by Stanley Chan on 2/2/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    NSMutableArray *squaresArray;
    NSMutableArray *labelArray;
}


@property (strong, nonatomic) NSMutableArray *squaresArray;
@property (strong, nonatomic) NSMutableArray *labelArray;

@property (strong, nonatomic) IBOutlet UIView *square0;
@property (strong, nonatomic) IBOutlet UIView *square1;
@property (strong, nonatomic) IBOutlet UIView *square2;
@property (strong, nonatomic) IBOutlet UIView *square3;
@property (strong, nonatomic) IBOutlet UIView *square4;
@property (strong, nonatomic) IBOutlet UIView *square5;
@property (strong, nonatomic) IBOutlet UIView *square6;
@property (strong, nonatomic) IBOutlet UIView *square7;
@property (strong, nonatomic) IBOutlet UIView *square8;
@property (strong, nonatomic) IBOutlet UIView *square9;
@property (strong, nonatomic) IBOutlet UIView *square10;
@property (strong, nonatomic) IBOutlet UIView *square11;
@property (strong, nonatomic) IBOutlet UIView *square12;
@property (strong, nonatomic) IBOutlet UIView *square13;
@property (strong, nonatomic) IBOutlet UIView *square14;
@property (strong, nonatomic) IBOutlet UIView *square15;

@property (strong, nonatomic) IBOutlet UILabel *label0;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UILabel *label4;
@property (strong, nonatomic) IBOutlet UILabel *label5;
@property (strong, nonatomic) IBOutlet UILabel *label6;
@property (strong, nonatomic) IBOutlet UILabel *label7;
@property (strong, nonatomic) IBOutlet UILabel *label8;
@property (strong, nonatomic) IBOutlet UILabel *label9;
@property (strong, nonatomic) IBOutlet UILabel *label10;
@property (strong, nonatomic) IBOutlet UILabel *label11;
@property (strong, nonatomic) IBOutlet UILabel *label12;
@property (strong, nonatomic) IBOutlet UILabel *label13;
@property (strong, nonatomic) IBOutlet UILabel *label14;
@property (strong, nonatomic) IBOutlet UILabel *label15;

@property (strong, nonatomic) IBOutlet UIButton *upButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *downButton;

@property (strong, nonatomic) IBOutlet UIButton *theNewGameButton;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end


