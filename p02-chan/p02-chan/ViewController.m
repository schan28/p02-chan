//
//  ViewController.m
//  p02-chan
//
//  Created by Stanley Chan on 2/2/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()
@end

@implementation ViewController
@synthesize squaresArray;
@synthesize labelArray;

//need to determine if the winning message has been displayed or not
bool achieved2048 = false;

- (int) randomNumberGen{
    int num = arc4random() % 16;
    while([[labelArray[num] text] intValue] != 0){
        num = arc4random() % 16;
    }
    [labelArray[num] setHidden: FALSE];
    return num;
}

- (void) colorChanger: (int)index currentNum:(int) num{
    //takes the number before its multiplied by 2
    UIView *s = squaresArray[index];
    switch(num){
        case 0:
            s.backgroundColor = [UIColor whiteColor];
            break;
        case 2:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (153/255.0) blue:(153/255.0) alpha:1];
            break;
        case 4:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (204/255.0) blue:(153/255.0) alpha:1];
            break;
        case 8:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (255/255.0) blue:(153/255.0) alpha:1];
            break;
        case 16:
            s.backgroundColor = [UIColor colorWithRed:(204/255.0) green: (255/255.0) blue:(153/255.0) alpha:1];
            break;
        case 32:
            s.backgroundColor = [UIColor colorWithRed:(153/255.0) green: (255/255.0) blue:(153/255.0) alpha:1];
            break;
        case 64:
            s.backgroundColor = [UIColor colorWithRed:(153/255.0) green: (255/255.0) blue:(204/255.0) alpha:1];
            break;
        case 128:
            s.backgroundColor = [UIColor colorWithRed:(153/255.0) green: (255/255.0) blue:(255/255.0) alpha:1];
            break;
        case 256:
            s.backgroundColor = [UIColor colorWithRed:(153/255.0) green: (204/255.0) blue:(255/255.0) alpha:1];
            break;
        case 512:
            s.backgroundColor = [UIColor colorWithRed:(153/255.0) green: (153/255.0) blue:(255/255.0) alpha:1];
            break;
        case 1024:
            s.backgroundColor = [UIColor colorWithRed:(204/255.0) green: (153/255.0) blue:(255/255.0) alpha:1];
            break;
        case 2048:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (153/255.0) blue:(255/255.0) alpha:1];
            //this is to ensure that the victory message doesnt pop up more than once per new game
            if(!achieved2048){
                [self gameWonAlert];
                achieved2048 = true;
            }
            break;
        case 4096:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (153/255.0) blue:(204/255.0) alpha:1];
            break;
        case 8192:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (102/255.0) blue:(178/255.0) alpha:1];
            break;
        case 16384:
            s.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (51/255.0) blue:(153/255.0) alpha:1];
            break;
        default:
            break;
    }
}

//this method is used to check if the game is over
- (bool) isGameOver{
    //check to see if all labels are filled
    for(int i = 0; i < 16; i++){
        if([[labelArray[i] text] intValue] == 0){
            return false;
        }
    }
    
    //now we must check that merges are no longer possible
    //check up merges first
    for(int i = 0; i < 12; i++){
        int currentLabelValue = [[labelArray[i] text] intValue];
        int belowLabelValue = [[labelArray[i+4] text] intValue];
        if(currentLabelValue == belowLabelValue){
            return false;
        }
    }
    //up merges not possible so we check down merges
    for(int i = 15; i > 3; i--){
        int currentLabelValue = [[labelArray[i] text] intValue];
        int aboveLabelValue = [[labelArray[i-4] text] intValue];
        if(currentLabelValue == aboveLabelValue){
            return false;
        }
    }
    //down merges also not possible so we check right merges
    for(int i = 15; i > 0; i-=4){
        for(int j = i; j > (i-3); j--){
            int currentLabelValue = [[labelArray[j] text] intValue];
            int leftLabelValue = [[labelArray[j-1] text] intValue];
            if(currentLabelValue == leftLabelValue){
                return false;
            }
        }
    }
    //right merges also not possible so we check left merges
    for(int i = 12; i >= 0; i-=4){
        for(int j = i; j < (i+3); j++){
            int currentLabelValue = [[labelArray[j] text] intValue];
            int rightLabelValue = [[labelArray[j+1] text] intValue];
            if(currentLabelValue == rightLabelValue){
                return false;
            }
        }
    }
    achieved2048 = false;
    return true;
}

- (void) gameWonAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congraluations on achieving 2048" message:@"You may either keep playing for a higher score or start a new game"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void) gameLostAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unforunately there are no more possible moves" message:@"Hit new game to start a new game"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (239/255.0) blue:(213/255.0) alpha:1];
    
    squaresArray =[NSMutableArray array];
    labelArray = [NSMutableArray array];

    [self.squaresArray addObject:_square0];
    [self.squaresArray addObject:_square1];
    [self.squaresArray addObject:_square2];
    [self.squaresArray addObject:_square3];
    [self.squaresArray addObject:_square4];
    [self.squaresArray addObject:_square5];
    [self.squaresArray addObject:_square6];
    [self.squaresArray addObject:_square7];
    [self.squaresArray addObject:_square8];
    [self.squaresArray addObject:_square9];
    [self.squaresArray addObject:_square10];
    [self.squaresArray addObject:_square11];
    [self.squaresArray addObject:_square12];
    [self.squaresArray addObject:_square13];
    [self.squaresArray addObject:_square14];
    [self.squaresArray addObject:_square15];
    
    [self.labelArray addObject:_label0];
    [self.labelArray addObject:_label1];
    [self.labelArray addObject:_label2];
    [self.labelArray addObject:_label3];
    [self.labelArray addObject:_label4];
    [self.labelArray addObject:_label5];
    [self.labelArray addObject:_label6];
    [self.labelArray addObject:_label7];
    [self.labelArray addObject:_label8];
    [self.labelArray addObject:_label9];
    [self.labelArray addObject:_label10];
    [self.labelArray addObject:_label11];
    [self.labelArray addObject:_label12];
    [self.labelArray addObject:_label13];
    [self.labelArray addObject:_label14];
    [self.labelArray addObject:_label15];
    for(int i = 0; i < [labelArray count]; i++){
        [labelArray[i] setHidden: TRUE];
        [labelArray[i] setText:[NSString stringWithFormat:@"%d", 0]];
        [labelArray[i] setTextAlignment:NSTextAlignmentCenter];
        [labelArray[i] setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
        [self colorChanger:i currentNum:0];
    }
  
    int num1 = [self randomNumberGen];
    [labelArray[num1] setText:[NSString stringWithFormat:@"%d", 2]];
    [self colorChanger:num1 currentNum:2];
    
    int num2 = [self randomNumberGen];
    [labelArray[num2] setText:[NSString stringWithFormat:@"%d", 2]];
    [self colorChanger:num2 currentNum:2];
    
    [_upButton setTitle:@"Up" forState:UIControlStateNormal];
    _upButton.backgroundColor = [UIColor colorWithRed:(170/255.0) green: (170/255.0) blue:(170/255.0) alpha:1];
    [_upButton.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    [_upButton setTitleColor:[UIColor colorWithRed:(24/255.0) green: (114/255.0) blue:(254/255.0) alpha:1]forState:UIControlStateNormal];
    _upButton.layer.cornerRadius = 7;
    _upButton.clipsToBounds = YES;

    [_rightButton setTitle:@"Right" forState:UIControlStateNormal];
    _rightButton.backgroundColor = [UIColor colorWithRed:(170/255.0) green: (170/255.0) blue:(170/255.0) alpha:1];
    [_rightButton.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    [_rightButton setTitleColor:[UIColor colorWithRed:(24/255.0) green: (114/255.0) blue:(254/255.0) alpha:1]forState:UIControlStateNormal];
    _rightButton.layer.cornerRadius = 7;
    _rightButton.clipsToBounds = YES;

    [_leftButton setTitle:@"Left" forState:UIControlStateNormal];
    _leftButton.backgroundColor = [UIColor colorWithRed:(170/255.0) green: (170/255.0) blue:(170/255.0) alpha:1];
    [_leftButton.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    [_leftButton setTitleColor:[UIColor colorWithRed:(24/255.0) green: (114/255.0) blue:(254/255.0) alpha:1]forState:UIControlStateNormal];
    _leftButton.layer.cornerRadius = 7;
    _leftButton.clipsToBounds = YES;

    [_downButton setTitle:@"Down" forState:UIControlStateNormal];
    _downButton.backgroundColor = [UIColor colorWithRed:(170/255.0) green: (170/255.0) blue:(170/255.0) alpha:1];
    [_downButton.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    [_downButton setTitleColor:[UIColor colorWithRed:(24/255.0) green: (114/255.0) blue:(254/255.0) alpha:1]forState:UIControlStateNormal];
    _downButton.layer.cornerRadius = 7;
    _downButton.clipsToBounds = YES;

    [_theNewGameButton setTitle:@"New Game" forState:UIControlStateNormal];
    _theNewGameButton.backgroundColor = [UIColor lightGrayColor];
    [_theNewGameButton.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    _theNewGameButton.layer.cornerRadius = 2;
    _theNewGameButton.clipsToBounds = YES;


    
    [_scoreLabel setText:[NSString stringWithFormat:@"%d", 0]];
    [_scoreLabel setTextAlignment:NSTextAlignmentCenter];
    _scoreLabel.backgroundColor = [UIColor lightGrayColor];
    [_scoreLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    _scoreLabel.layer.cornerRadius = 2;
    _scoreLabel.clipsToBounds = YES;

    
    [_scoreTextLabel setText:@"Score:"];
    [_scoreTextLabel setTextAlignment:NSTextAlignmentCenter];
    _scoreTextLabel.backgroundColor = [UIColor lightGrayColor];
    [_scoreTextLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18]];
    _scoreTextLabel.layer.cornerRadius = 2;
    _scoreTextLabel.clipsToBounds = YES;

    
    [_nameLabel setText:@"2048 Clone"];
    [_nameLabel setTextAlignment:NSTextAlignmentCenter];
    [_nameLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:36]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)upButton:(id)sender{
    //this is to make sure something did move or else we won't generate a new number
    int didMove = 0;
    
    //first we want to push all the numbers to the top then we will merge them from there
    
    for(int i = 0; i < 12; i++){
        int currentLabelValue = [[labelArray[i] text] intValue];
        int belowLabelValue = [[labelArray[i+4] text] intValue];
        //this means we can transfer the value in the lower block to our current block
        if(currentLabelValue == 0 && belowLabelValue != 0){
            //updates top block
            [labelArray[i] setText:[NSString stringWithFormat:@"%d", belowLabelValue]];
            [self colorChanger:i currentNum:belowLabelValue];
            [labelArray[i] setHidden: FALSE];
            //changes lower block back to 0
            [labelArray[i+4] setText:[NSString stringWithFormat:@"%d", 0]];
            [self colorChanger:(i+4) currentNum:0];
            [labelArray[i+4] setHidden: TRUE];

            //we do this because we will need to check our blocks again in case it needs to be moved up even more
            i = -1;
            didMove = 1;
        }
    }
    
    //now we do the merge
    for(int i = 0; i < 12; i++){
        int currentLabelValue = [[labelArray[i] text] intValue];
        int belowLabelValue = [[labelArray[i+4] text] intValue];
    
        //merge if the values are equal except if their values are 0
        if((currentLabelValue == belowLabelValue) && (currentLabelValue != 0)){

            //after the two merge the one below it will become a 0
            [labelArray[i+4] setText:[NSString stringWithFormat:@"%d",0]];
            [self colorChanger:(i+4) currentNum:0];
            [labelArray[i+4] setHidden: TRUE];

            //the one on top will now double in value
            [labelArray[i] setText:[NSString stringWithFormat:@"%d", currentLabelValue*2]];
            [self colorChanger:i currentNum:currentLabelValue*2];
            
            //update score value
            [_scoreLabel setText:[NSString stringWithFormat:@"%d", [[_scoreLabel text] intValue]+(currentLabelValue*2)]];
            
            //now we must move up the things below it
            for(int j = (i+4); j < 12; j+=4){
                [labelArray[j] setText:[NSString stringWithFormat:@"%d", [[labelArray[j+4] text] intValue]]];
                [self colorChanger:j currentNum:[[labelArray[j] text] intValue]];
                
                //we could be moving in another zero so we must check for this
                if([[labelArray[j] text] intValue] == 0){
                    [labelArray[j] setHidden: TRUE];
                }
                else{
                    [labelArray[j] setHidden: FALSE];
                }
                [labelArray[j+4] setText:[NSString stringWithFormat:@"%d", 0]];
                [self colorChanger:(j+4) currentNum:0];
                [labelArray[j+4] setHidden: TRUE];

            }
            didMove = 1;
        }
    }
    //to ensure that a movement did occur
    if(didMove){
        int num = [self randomNumberGen];
        [labelArray[num] setText:[NSString stringWithFormat:@"%d", 2]];
        [self colorChanger:num currentNum:2];
        if([self isGameOver]){
            [self gameLostAlert];
        }
    }
}

- (IBAction)rightButton:(id)sender{
    
    //this is to make sure something did move or else we wont generate a new number
    int didMove = 0;
    
    
    //first we push all the values to as far right as they can go then we merge later
    //this starts in rightmost column
    for(int i = 15; i > 0; i-=4){
        //and moves leftwards in the row
        for(int j = i; j > (i-3); j--){
            int currentLabelValue = [[labelArray[j] text] intValue];
            int leftLabelValue = [[labelArray[j-1] text] intValue];

            //this means we can transfer the value in the left block to the right block
            if(currentLabelValue == 0 && leftLabelValue != 0){
                //updates right block
                [labelArray[j] setText:[NSString stringWithFormat:@"%d", leftLabelValue]];
                [self colorChanger:j currentNum:leftLabelValue];
                [labelArray[j] setHidden: FALSE];

                //changes left block back to 0
                [labelArray[j-1] setText:[NSString stringWithFormat:@"%d", 0]];
                [self colorChanger:(j-1) currentNum:0];
                [labelArray[j-1] setHidden: TRUE];

                j = i+1;
                didMove = 1;
            }
        }
    }
    
    //the merging starts here
    for(int i = 15; i > 0; i-=4){
        for(int j = i; j > (i-3); j--){
            int currentLabelValue = [[labelArray[j] text] intValue];
            int leftLabelValue = [[labelArray[j-1] text] intValue];
            //merge if the values are equal except if their values are 0
            if((currentLabelValue == leftLabelValue) && (currentLabelValue != 0)){

                //right block doubles in value
                [labelArray[j] setText:[NSString stringWithFormat:@"%d", currentLabelValue*2]];
                [self colorChanger:j currentNum:currentLabelValue*2];
                
                //left block becomes 0
                [labelArray[j-1] setText:[NSString stringWithFormat:@"%d",0]];
                [self colorChanger:(j-1) currentNum:0];
                [labelArray[j-1] setHidden: TRUE];

                //update score value
                [_scoreLabel setText:[NSString stringWithFormat:@"%d", [[_scoreLabel text] intValue]+(currentLabelValue*2)]];
                
                //now we must move to the right the things to the left of it
                for(int k = (j-1); k > (i-3); k--){
                    [labelArray[k] setText:[NSString stringWithFormat:@"%d", [[labelArray[k-1] text] intValue]]];
                    [self colorChanger:k currentNum:[[labelArray[k] text] intValue]];
                    
                    //we could be moving in another zero so we must check for this
                    if([[labelArray[k] text] intValue] == 0){
                        [labelArray[k] setHidden: TRUE];
                    }
                    else{
                        [labelArray[k] setHidden: FALSE];
                    }

                    [labelArray[k-1] setText:[NSString stringWithFormat:@"%d", 0]];
                    [self colorChanger:(k-1) currentNum:0];
                    [labelArray[k-1] setHidden: TRUE];

                }
                didMove = 1;
            }
        }
    }
    //to ensure that a movement did occur
    if(didMove){
        int num = [self randomNumberGen];
        [labelArray[num] setText:[NSString stringWithFormat:@"%d", 2]];
        [self colorChanger:num currentNum:2];
        if([self isGameOver]){
            [self gameLostAlert];
        }
    }
}

- (IBAction)leftButton:(id)sender{
    
    //this is to make sure something did move or else we wont generate a new number
    int didMove = 0;
    
    //move blocks as leftmost as possible
    //this starts in the leftmost column
    for(int i = 12; i >= 0; i-=4){
        //and moves rightwards in the row
        for(int j = i; j < (i+3); j++){
            int currentLabelValue = [[labelArray[j] text] intValue];
            int rightLabelValue = [[labelArray[j+1] text] intValue];
            
            //this means we can transfer the value in the right block to the left block
            if(currentLabelValue == 0 && rightLabelValue != 0){
                //updates left block
                [labelArray[j] setText:[NSString stringWithFormat:@"%d", rightLabelValue]];
                [self colorChanger:j currentNum:rightLabelValue];
                [labelArray[j] setHidden: FALSE];
                //changes right block back to 0
                [labelArray[j+1] setText:[NSString stringWithFormat:@"%d", 0]];
                [self colorChanger:(j+1) currentNum:0];
                [labelArray[j+1] setHidden: TRUE];
                j = i-1;
                didMove = 1;
            }
        }
    }

    //the merging starts here
    for(int i = 12; i >= 0; i-=4){
        for(int j = i; j < (i+3); j++){
            int currentLabelValue = [[labelArray[j] text] intValue];
            int rightLabelValue = [[labelArray[j+1] text] intValue];
            //merge if the values are equal except if their values are 0
            if((currentLabelValue == rightLabelValue) && (currentLabelValue != 0)){

                //left block doubles in value
                [labelArray[j] setText:[NSString stringWithFormat:@"%d", currentLabelValue*2]];
                [self colorChanger:j currentNum:currentLabelValue*2];
               
                //right block becomes 0
                [labelArray[j+1] setText:[NSString stringWithFormat:@"%d",0]];
                [self colorChanger:(j+1) currentNum:0];
                [labelArray[j+1] setHidden: TRUE];

                //update score value
                [_scoreLabel setText:[NSString stringWithFormat:@"%d", [[_scoreLabel text] intValue]+(currentLabelValue*2)]];
                
                //now we must move to the left the things to the right of it
                for(int k = (j+1); k < (i+3); k++){
                    [labelArray[k] setText:[NSString stringWithFormat:@"%d", [[labelArray[k+1] text] intValue]]];
                    [self colorChanger:k currentNum:[[labelArray[k] text] intValue]];
                    //we could be moving in another zero so we must check for this
                    if([[labelArray[k] text] intValue] == 0){
                        [labelArray[k] setHidden: TRUE];
                    }
                    else{
                        [labelArray[k] setHidden: FALSE];
                    }
                    
                    [labelArray[k+1] setText:[NSString stringWithFormat:@"%d", 0]];
                    [self colorChanger:(k+1) currentNum:0];
                    [labelArray[k+1] setHidden: TRUE];
                }
                didMove = 1;
            }
        }
    }
    //to ensure that a movement did occur
    if(didMove){
        int num = [self randomNumberGen];
        [labelArray[num] setText:[NSString stringWithFormat:@"%d", 2]];
        [self colorChanger:num currentNum:2];
        if([self isGameOver]){
            [self gameLostAlert];
        }
    }

}

- (IBAction)downButton:(id)sender{
    
    //this is to make sure something did move or else we won't generate a new number
    int didMove = 0;
    
    //first we want to push all the numbers to the bottom then we will merge them from there
    for(int i = 15; i > 3; i--){
        int currentLabelValue = [[labelArray[i] text] intValue];
        int aboveLabelValue = [[labelArray[i-4] text] intValue];
        //this means we can transfer the value in the upper block to our lower block
        if(currentLabelValue == 0 && aboveLabelValue != 0){
            //updates current block
            [labelArray[i] setText:[NSString stringWithFormat:@"%d", aboveLabelValue]];
            [self colorChanger:i currentNum:aboveLabelValue];
            [labelArray[i] setHidden: FALSE];
            //changes above block back to 0
            [labelArray[i-4] setText:[NSString stringWithFormat:@"%d", 0]];
            [self colorChanger:(i-4) currentNum:0];
            [labelArray[i-4] setHidden: TRUE];

            //we do this because we will need to check our blocks again in case it needs to be moved down even more
            i = 16;
            didMove = 1;
        }
    }
    
    //now we do the merge
    for(int i = 15; i > 3; i--){
        int currentLabelValue = [[labelArray[i] text] intValue];
        int aboveLabelValue = [[labelArray[i-4] text] intValue];
        
        //merge if the values are equal except if their values are 0
        if((currentLabelValue == aboveLabelValue) && (currentLabelValue != 0)){
            //after the two merge the one above will become a 0
            [labelArray[i-4] setText:[NSString stringWithFormat:@"%d",0]];
            [self colorChanger:(i-4) currentNum:0];
            [labelArray[i-4] setHidden: TRUE];

            //the one on bottom will now double in value
            [labelArray[i] setText:[NSString stringWithFormat:@"%d", (currentLabelValue*2)]];
            [self colorChanger:i currentNum:(currentLabelValue*2)];
            
            //update score value
            [_scoreLabel setText:[NSString stringWithFormat:@"%d", [[_scoreLabel text] intValue]+(currentLabelValue*2)]];
            
            //now we must move down the things above it
            for(int j = (i-4); j > 3; j-=4){
                [labelArray[j] setText:[NSString stringWithFormat:@"%d", [[labelArray[j-4] text] intValue]]];
                [self colorChanger:j currentNum:[[labelArray[j] text] intValue]];
                //we could be moving in another zero so we must check for this
                if([[labelArray[j] text] intValue] == 0){
                    [labelArray[j] setHidden: TRUE];
                }
                else{
                    [labelArray[j] setHidden: FALSE];
                }

                [labelArray[j-4] setText:[NSString stringWithFormat:@"%d", 0]];
                [self colorChanger:(j-4) currentNum:0];
                [labelArray[j-4] setHidden: TRUE];

            }
            didMove = 1;
        }
    }
    //to ensure that a movement did occur
    if(didMove){
        int num = [self randomNumberGen];
        [labelArray[num] setText:[NSString stringWithFormat:@"%d", 2]];
        [self colorChanger:num currentNum:2];
        if([self isGameOver]){
            [self gameLostAlert];
        }
    }
}

- (IBAction)theNewGameButton:(id)sender{
    //resets all the labels to 0
    for(int i = 0; i < 16; i++){
        [labelArray[i] setHidden: TRUE];
        [labelArray[i] setText:[NSString stringWithFormat:@"%d", 0]];
        [self colorChanger:i currentNum:0];
    }
    //randomly chooses 2 blocks to have the value 2
    int num1 = [self randomNumberGen];
    [labelArray[num1] setText:[NSString stringWithFormat:@"%d", 2]];
    [self colorChanger:num1 currentNum:2];
    
    int num2 = [self randomNumberGen];
    [labelArray[num2] setText:[NSString stringWithFormat:@"%d", 2]];
    [self colorChanger:num2 currentNum:2];
    
    [_scoreLabel setText:[NSString stringWithFormat:@"%d", 0]];
    
    achieved2048 = false;
}


@end
